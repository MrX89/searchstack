package com.kubak89.searchstack.repository

import com.kubak89.searchstack.data.SearchResult
import com.kubak89.searchstack.database.StackDatabase
import com.kubak89.searchstack.rest.StackOverflowApi
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import javax.inject.Inject

class SearchResultsRepository @Inject constructor(private val stackOverflowApi: StackOverflowApi, private val database: StackDatabase) {
    fun getSearchResults(searchQuery: String): Observable<ArrayList<SearchResult>> = stackOverflowApi.
            searchQuestions(searchQuery).
            subscribeOn(Schedulers.io()).
            map { it.items }.
            doOnNext {
                database.searchResultDao().insertAll(it)
            }.
            onErrorResumeNext { t: Throwable ->
                ignoreIOException(t)
            }.
            map { getSearchResultsFromDB(searchQuery) }

    private fun ignoreIOException(throwable: Throwable): ObservableSource<out ArrayList<SearchResult>> {
        return when (throwable) {
            is IOException -> Observable.just(ArrayList())
            else -> Observable.error(throwable)
        }
    }

    private fun getSearchResultsFromDB(query: String) = ArrayList(database.searchResultDao().searchResults("%$query%"))
}