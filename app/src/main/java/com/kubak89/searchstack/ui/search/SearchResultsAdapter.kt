package com.kubak89.searchstack.ui.search

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.kubak89.searchstack.data.SearchResult
import com.kubak89.searchstack.databinding.ItemSearchResultBinding
import com.kubak89.searchstack.ui.QuestionDetailsActivity

class SearchResultsAdapter(private val context: Context) : RecyclerView.Adapter<SearchResultViewHolder>() {
    var searchResults = ArrayList<SearchResult>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: SearchResultViewHolder?, position: Int) {
        holder?.apply {
            val item = searchResults[position]
            bindSearchResult(item)
            binding.root.setOnClickListener {
                QuestionDetailsActivity.start(context,
                        item.link,
                        item.title)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val binding = ItemSearchResultBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchResultViewHolder(binding)
    }

    override fun getItemCount() = searchResults.count()
}

class SearchResultViewHolder(val binding: ItemSearchResultBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bindSearchResult(searchResult: SearchResult) {
        binding.searchResult = searchResult
    }
}