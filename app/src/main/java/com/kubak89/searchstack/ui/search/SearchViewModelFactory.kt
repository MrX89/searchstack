package com.kubak89.searchstack.ui.search

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.kubak89.searchstack.SearchStackApplication
import com.kubak89.searchstack.repository.SearchResultsRepository
import javax.inject.Inject

class SearchViewModelFactory(searchStackApplication: SearchStackApplication) : ViewModelProvider.Factory {
    @Inject
    lateinit var searchResultsRepository: SearchResultsRepository

    init {
        searchStackApplication.repoComponent.inject(this)
    }


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SearchViewModel(searchResultsRepository) as T
        } else {
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}