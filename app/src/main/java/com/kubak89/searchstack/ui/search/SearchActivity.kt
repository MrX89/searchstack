package com.kubak89.searchstack.ui.search

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import com.kubak89.searchstack.R
import com.kubak89.searchstack.SearchStackApplication
import kotlinx.android.synthetic.main.activity_search.*

private const val KEY_RESULTS = "results"
private const val KEY_QUERY = "query"

class SearchActivity : AppCompatActivity() {
    private val adapter = SearchResultsAdapter(this)
    private lateinit var viewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        searchResults.adapter = adapter

        val searchViewModelFactory = SearchViewModelFactory(application as SearchStackApplication)
        viewModel = ViewModelProviders.of(this, searchViewModelFactory).get(SearchViewModel::class.java)

        restoreViewModelState(savedInstanceState)

        bindViewModel()
    }

    private fun restoreViewModelState(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            viewModel.searchQuery = it.getString(KEY_QUERY, "")
            viewModel.setSearchItems(it.getParcelableArrayList(KEY_RESULTS))
        }
    }

    private fun bindViewModel() {
        queryText.apply {
            setText(viewModel.searchQuery)
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable) {
                    viewModel.searchQueryObservable.onNext(p0.toString())
                    swipeRefreshLayout.isRefreshing = true
                }
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            })
        }

        viewModel.errorsLiveData.observe(this, Observer {
            it?.let {
                Snackbar.make(content, it.toString(), Snackbar.LENGTH_LONG).show()
                swipeRefreshLayout.isRefreshing = false
            }
        })

        viewModel.searchResultsLiveData.observe(this, Observer {
            it?.let {
                adapter.searchResults = it
                swipeRefreshLayout.isRefreshing = false
            }
        })

        swipeRefreshLayout.setOnRefreshListener { viewModel.loadingObservable.onNext(Unit) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putParcelableArrayList(KEY_RESULTS, viewModel.searchResultsLiveData.value)
            putString(KEY_QUERY, viewModel.searchQuery)
        }
    }
}