package com.kubak89.searchstack.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.webkit.WebView

private const val QUESTION_URL = "URL"
private const val QUESTION_TITLE = "QUESTION_TITLE"

class QuestionDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val webview = WebView(this)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = intent.getStringExtra(QUESTION_TITLE)
        }
        setContentView(webview)
        webview.loadUrl(intent.getStringExtra(QUESTION_URL))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context, questionPageUrl: String, questionTitle: String) {
            val intent = Intent(context, QuestionDetailsActivity::class.java)
            intent.putExtra(QUESTION_URL, questionPageUrl)
            intent.putExtra(QUESTION_TITLE, questionTitle)
            context.startActivity(intent)
        }
    }
}