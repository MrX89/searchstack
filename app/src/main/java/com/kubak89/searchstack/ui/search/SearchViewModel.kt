package com.kubak89.searchstack.ui.search

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.kubak89.searchstack.appComponents.SingleLiveEvent
import com.kubak89.searchstack.data.SearchResult
import com.kubak89.searchstack.repository.SearchResultsRepository
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchViewModel @Inject constructor(private val searchResultRepository: SearchResultsRepository) : ViewModel() {
    private var searchQueryDisposable: Disposable
    var searchQuery = ""

    val errorsLiveData = SingleLiveEvent<Throwable>()
    val searchResultsLiveData = MutableLiveData<ArrayList<SearchResult>>()
    val searchQueryObservable: PublishSubject<String> = PublishSubject.create<String>()
    val loadingObservable: PublishSubject<Unit> = PublishSubject.create()

    init {
        val searchQueryObservable = searchQueryObservable.debounce(2, TimeUnit.SECONDS)
        val refreshObservable = loadingObservable.map { searchQuery }
        searchQueryDisposable = Observable
                .merge(searchQueryObservable, refreshObservable)
                .doOnNext { searchQuery = it }
                .flatMap {
                    if (it.isNotEmpty()) {
                        searchResultRepository.getSearchResults(searchQuery)
                                .onErrorResumeNext({ t: Throwable ->
                                    t.printStackTrace()
                                    errorsLiveData.postValue(t)
                                    Observable.empty()
                                })
                    } else {
                        Observable.just(ArrayList())
                    }
                }
                .subscribe({ searchResults: ArrayList<SearchResult> ->
                    searchResultsLiveData.postValue(searchResults)
                })
    }

    override fun onCleared() {
        searchQueryDisposable.dispose()
    }

    fun setSearchItems(searchResults: ArrayList<SearchResult>?) {
        if (searchResults != null)
            searchResultsLiveData.postValue(searchResults)
    }
}