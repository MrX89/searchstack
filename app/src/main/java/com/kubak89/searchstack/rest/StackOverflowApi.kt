package com.kubak89.searchstack.rest

import com.kubak89.searchstack.data.StackApiResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface StackOverflowApi {
    @GET("search?order=desc&sort=activity&site=stackoverflow")
    fun searchQuestions(@Query("intitle") searchQuery: String): Observable<StackApiResponse>
}