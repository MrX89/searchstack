package com.kubak89.searchstack.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.kubak89.searchstack.data.Owner
import com.kubak89.searchstack.data.SearchResult


@Database(entities = arrayOf(Owner::class, SearchResult::class), version = 1)
abstract class StackDatabase : RoomDatabase() {
    abstract fun searchResultDao(): SearchResultDao
}