package com.kubak89.searchstack.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.kubak89.searchstack.data.SearchResult

@Dao
interface SearchResultDao {
    @Query("SELECT * FROM searchresult WHERE title LIKE :searchQuery")
    fun searchResults(searchQuery : String) : List<SearchResult>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(searchResults: ArrayList<SearchResult>)
}