package com.kubak89.searchstack.di.components

import com.kubak89.searchstack.di.modules.AppModule
import com.kubak89.searchstack.di.modules.DatabaseModule
import com.kubak89.searchstack.di.modules.RestModule
import com.kubak89.searchstack.ui.search.SearchViewModelFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, RestModule::class, DatabaseModule::class))
interface RepoComponent {
    fun inject(searchViewModelFactory: SearchViewModelFactory)
}

