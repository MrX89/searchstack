package com.kubak89.searchstack.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import com.kubak89.searchstack.database.StackDatabase
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {
    @Provides
    fun provideDatabase(application: Application) = Room.
            databaseBuilder(application, StackDatabase::class.java, "stackDatabase").
            build()
}