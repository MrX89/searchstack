package com.kubak89.searchstack.databinding

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.kubak89.searchstack.R


@BindingAdapter("srcUrl")
fun bindSourceUrl(view: ImageView, url: String?) {
    val requestOptions = RequestOptions()
            .placeholder(R.drawable.ic_person)

    Glide.with(view.context).setDefaultRequestOptions(requestOptions).load(url).
            into(view)
}