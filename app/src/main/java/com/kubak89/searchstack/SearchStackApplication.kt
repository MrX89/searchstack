package com.kubak89.searchstack

import android.app.Application
import com.kubak89.searchstack.di.components.DaggerRepoComponent
import com.kubak89.searchstack.di.components.RepoComponent
import com.kubak89.searchstack.di.modules.AppModule
import com.kubak89.searchstack.di.modules.DatabaseModule
import com.kubak89.searchstack.di.modules.RestModule

class SearchStackApplication : Application() {
    val repoComponent: RepoComponent = DaggerRepoComponent.
            builder().
            appModule(AppModule(this)).
            restModule(RestModule()).
            databaseModule(DatabaseModule()).
            build()
}