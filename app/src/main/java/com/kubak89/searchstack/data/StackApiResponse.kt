package com.kubak89.searchstack.data

import com.google.gson.annotations.SerializedName

data class StackApiResponse(val items: ArrayList<SearchResult> = arrayListOf(),
                            @SerializedName("has_more") val hasMore: Boolean = false,
                            @SerializedName("quota_max") val quotaMax: Int = 0,
                            @SerializedName("quota_remaining") val quotaRemaining: Int = 0)