package com.kubak89.searchstack.data

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Entity
data class SearchResult(@Embedded val owner: Owner,
                        @SerializedName("answer_count") val answerCount: Int,
                        @PrimaryKey @SerializedName("question_id") val questionId: Int,
                        val link: String,
                        val title: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Owner::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(owner, flags)
        parcel.writeInt(answerCount)
        parcel.writeInt(questionId)
        parcel.writeString(link)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchResult> {
        override fun createFromParcel(parcel: Parcel): SearchResult {
            return SearchResult(parcel)
        }

        override fun newArray(size: Int): Array<SearchResult?> {
            return arrayOfNulls(size)
        }
    }


}
