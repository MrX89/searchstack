package com.kubak89.searchstack.ui.search

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.kubak89.searchstack.data.Owner
import com.kubak89.searchstack.data.SearchResult
import com.kubak89.searchstack.repository.SearchResultsRepository
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import junit.framework.Assert.assertEquals
import org.awaitility.Awaitility.await
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers.anyString
import java.util.concurrent.TimeUnit


class SearchViewModelTest {
    private val searchResults = arrayListOf(SearchResult(Owner(0, 0, "link", "Name"), 0, 0, "", ""))

    @Suppress("unused")
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun searchResultsDelivered() {
        val repositoryMock = createRepositoryMockReturningResults()
        val tested = createTested(repositoryMock)

        tested.searchQueryObservable.onNext("Search query")

        await().atMost(3, TimeUnit.SECONDS).until { tested.searchResultsLiveData.value == searchResults }
    }

    @Test
    fun pullToRefreshTriggersSearch() {
        val repositoryMock = createRepositoryMockReturningResults()
        val tested = createTested(repositoryMock)

        tested.searchQuery = "test"
        tested.loadingObservable.onNext(Unit)

        await().
                atMost(3, TimeUnit.SECONDS).
                until { tested.searchResultsLiveData.value == searchResults }
    }

    @Test
    fun searchInputIsDebounced() {
        val repositoryMock = createRepositoryMockReturningResults()
        val tested = createTested(repositoryMock)

        tested.searchQueryObservable.onNext("Search query")

        assertEquals(null, tested.searchResultsLiveData.value)
    }

    @Test
    fun errorDelivered() {
        val exception = Exception("Test exception - this message should be printed.")
        val repositoryMock = createRepoistoryReturningException(exception)
        val tested = createTested(repositoryMock)

        tested.searchQueryObservable.onNext("query")

        await().atMost(3, TimeUnit.SECONDS).until { tested.errorsLiveData.value == exception}
    }

    private fun createTested(repositoryMock: SearchResultsRepository): SearchViewModel {
        val tested = SearchViewModel(repositoryMock)
        val observer = mock<Observer<ArrayList<SearchResult>>>()
        tested.searchResultsLiveData.observeForever(observer)
        return tested
    }

    private fun createRepositoryMockReturningResults(): SearchResultsRepository = mock {
        on { getSearchResults(anyString()) } doReturn Observable.just(searchResults)
    }

    private fun createRepoistoryReturningException(exception: Exception): SearchResultsRepository = mock {
        on { getSearchResults(anyString()) } doReturn Observable.error(exception)
    }
}