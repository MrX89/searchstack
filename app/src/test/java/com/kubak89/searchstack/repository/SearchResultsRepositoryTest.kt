package com.kubak89.searchstack.repository

import com.kubak89.searchstack.data.Owner
import com.kubak89.searchstack.data.SearchResult
import com.kubak89.searchstack.data.StackApiResponse
import com.kubak89.searchstack.database.SearchResultDao
import com.kubak89.searchstack.database.StackDatabase
import com.kubak89.searchstack.rest.StackOverflowApi
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import java.io.IOException
import java.util.concurrent.TimeUnit

class SearchResultsRepositoryTest {
    private val responseItems = arrayListOf(SearchResult(Owner(0, 0, "link", "Name"), 0, 0, "", ""))
    private val response = StackApiResponse(responseItems)

    private val databaseItems = arrayListOf(SearchResult(Owner(1, 1, "link", "Name"), 1, 1, "", ""))

    @Test
    fun dataIsSavedToDB() {
        val (mockDao, tested) = createTested(Observable.just(response))

        val testObserver = createTestObserver()
        tested.getSearchResults("query").subscribe(testObserver)
        testObserver.awaitCount(1)

        verify(mockDao).insertAll(responseItems)
    }

    @Test
    fun dataIsAlwaysFetchedFromDB() {
        val (_, tested) = createTested(Observable.just(response))

        val testObserver = createTestObserver()
        tested.getSearchResults("query").subscribe(testObserver)

        testObserver.awaitCount(1).assertValue { it == databaseItems }
    }

    @Test
    fun databaseItemsFetchedOnIoException() {
        val (_, tested) = createTested(Observable.error(IOException()))

        val testObserver = createTestObserver()
        tested.getSearchResults("query").subscribe(testObserver)

        testObserver.awaitCount(1).assertResult(databaseItems)
    }

    @Test
    fun errorPropagated() {
        val expectedException = Exception()
        val (_, tested) = createTested(Observable.error(expectedException))

        val testObserver = createTestObserver()
        tested.getSearchResults("query").subscribe(testObserver)
        testObserver.awaitTerminalEvent(3, TimeUnit.SECONDS)

        testObserver.assertError(expectedException)
    }

    private fun createTestObserver() = TestObserver<ArrayList<SearchResult>>()

    private fun createTested(apiReturn: Observable<StackApiResponse>): Pair<SearchResultDao, SearchResultsRepository> {
        val apiMock = mock<StackOverflowApi> {
            on { searchQuestions(anyString()) } doReturn apiReturn
        }
        val mockDao = mock<SearchResultDao> {
            on { searchResults(anyString()) } doReturn databaseItems
        }
        val databaseMock = mock<StackDatabase> {
            on { searchResultDao() } doReturn mockDao
        }
        val tested = SearchResultsRepository(apiMock, databaseMock)
        return Pair(mockDao, tested)
    }
}